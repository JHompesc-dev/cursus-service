package nl.kzaconnected.cursus.repository;

import nl.kzaconnected.cursus.model.Dao.Cursus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CursusRepository extends JpaRepository<Cursus,Long> {}
