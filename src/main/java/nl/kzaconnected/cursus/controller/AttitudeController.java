package nl.kzaconnected.cursus.controller;

import nl.kzaconnected.cursus.model.Dao.Attitude;
import nl.kzaconnected.cursus.service.AttitudeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/attitudes")
@CrossOrigin
public class AttitudeController {

    @Autowired
    private AttitudeService attitudeService;

    @GetMapping
    public List<Attitude> getAllAttitudes() {
        return attitudeService.findAll();
    }
}


